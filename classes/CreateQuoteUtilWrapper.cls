public class CreateQuoteUtilWrapper {
	public class PickListWapper{
		@AuraEnabled public list<PickListDetails> quoteRecordTypePicklist{
			get;set;
		}
        @AuraEnabled public list<PickListDetails> commercialCoverageTypePicklist{
			get;set;
		}
        @AuraEnabled public list<PickListDetails> agricultureCoverageTypePicklist{
			get;set;
		}
        @AuraEnabled public list<PickListDetails> accountStatusTypePicklist{
			get;set;
		}
		@AuraEnabled public list<PickListDetails> quotedTermPicklist{
			get;set;
		}
        @AuraEnabled public list<PickListDetails> additionalPropertyPicklist{
			get;set;
		}
		@AuraEnabled public list<PickListDetails> additionalInsuredPickList{
			get;set;
		}
		@AuraEnabled public list<PickListDetails> mailingIsPropertyPickList{
			get;set;
		}
		@AuraEnabled public list<PickListDetails> applicantPickList{
			get;set;
		}
		@AuraEnabled public list<PickListDetails> salutationPickList{
			get;set;
		}
		@AuraEnabled public list<PickListDetails> includedExcludedPickList{
			get;set;
		}
		@AuraEnabled public list<PickListDetails> relationshipPickList{
			get;set;
		}
		@AuraEnabled public list<PickListDetails>  maritalStatusPickList{
			get;set;
		}
		@AuraEnabled public list<PickListDetails> genderPickList{
			get;set;
		}

	}
	public class PickListDetails{
		@AuraEnabled
		public String label = '';
		@AuraEnabled
		public String value = '';
	}
	public class AccountDetails{
		@AuraEnabled
		public String Name = '';
		@AuraEnabled
		public String RecordTypeId = '';
		@AuraEnabled
		public String Phone2 = '';
		@AuraEnabled
		public String Email = '';
		@AuraEnabled
		public String Fax = '';
		@AuraEnabled
		public String CellPhone = '';
		@AuraEnabled
		public String BillingAddress = '';
		@AuraEnabled
		public String BillingCity = '';
		@AuraEnabled
		public String BillingState = '';
		@AuraEnabled
		public String BillingPostalCode = '';
		@AuraEnabled
		public String BillingCountry = '';
		@AuraEnabled
		public Boolean UseMailingAddress = false;
		@AuraEnabled
		public String ShippingAddress = '';
		@AuraEnabled
		public String ShippingCity = '';
		@AuraEnabled
		public String ShippingState = '';
		@AuraEnabled
		public String ShippingPostalCode = '';
		@AuraEnabled
		public String ShippingCountry = '';

	}
	public class ContactDetails{
		@AuraEnabled
		public String LastName = '';
		@AuraEnabled
		public String FirstName = '';
		@AuraEnabled
		public String RecordTypeId = '';
	}
	public class InsuredDetails{
		@AuraEnabled
		public String Name = '';
		@AuraEnabled
		public String RecordTypeId = '';
		@AuraEnabled
		public String CompanyDriverNumber= '';
		@AuraEnabled
		public String Applicant = '';
		@AuraEnabled
		public String Salutation = '';
		@AuraEnabled
		public String IncludedExcluded = '';
		@AuraEnabled
		public String FirstName = '';
		@AuraEnabled
		public String MiddleName = '';
		@AuraEnabled
		public String LastName = '';
		@AuraEnabled
		public String Suffix = '';
		@AuraEnabled
		public String Phone = '';
		@AuraEnabled
		public String Type = '';
		@AuraEnabled
		public String Address = '';
		@AuraEnabled
		public String Email = '';
		@AuraEnabled
		public String City = '';
		@AuraEnabled
		public String State = '';
		@AuraEnabled
		public String ZipCode = '';
		@AuraEnabled
		public String DateOfBirth = '';
		@AuraEnabled
		public String Relationship = '';
		@AuraEnabled
		public String MaritalStatus = '';
		@AuraEnabled
		public String Gender = '';
	}
	public class InsuranceQuoteDetails{
		@AuraEnabled
		public String Name = '';
		@AuraEnabled
		public String QuoteRecordType= '';
		@AuraEnabled
		public String PolicyEffectiveDate = '';
		@AuraEnabled
		public String QuotedTerm = '';
		@AuraEnabled
		public String AdditionalInsured = '';
		@AuraEnabled
		public String MailingIsProperty = '';
		@AuraEnabled
		public String Additionalproperty = '';
	}
	public class propertyDetails{
		@AuraEnabled
		public String Name = '';
		@AuraEnabled
		public String PropertyAddress2 = '';
		@AuraEnabled
		public String City = '';
		@AuraEnabled
		public String State = '';
		@AuraEnabled
		public String ZipCode = '';
		@AuraEnabled
		public String County = '';
	}
}