public class CreateQuoteUtilClass {
    
    public static CreateQuoteUtilWrapper.PickListWapper getAllPickListRecord() {
        try {
            CreateQuoteUtilWrapper.PickListWapper wrapperobj = new CreateQuoteUtilWrapper.PickListWapper();
            
            wrapperobj.quoteRecordTypePicklist = getPicklistValues('CanaryAMS__Insurance_Product__c','Quote_Record_Type__c');
            wrapperobj.quotedTermPicklist = getPicklistValues('CanaryAMS__Insurance_Product__c', 'CanaryAMS__Quoted_Term__c');
            wrapperobj.commercialCoverageTypePicklist = getPicklistValues('CanaryAMS__Insurance_Product__c', 'CS_Commercial_Coverage_Type__c');
            wrapperobj.additionalInsuredPickList = getPicklistValues('CanaryAMS__Insurance_Product__c', 'CS_Additional_Insured__c');
            wrapperobj.mailingIsPropertyPickList = getPicklistValues('CanaryAMS__Insurance_Product__c', 'CS_Mailing_is_Property__c');
            wrapperobj.applicantPickList = getPicklistValues('CanaryAMS__Insured__c', 'CanaryAMS__Applicant__c');
            wrapperobj.salutationPickList = getPicklistValues('CanaryAMS__Insured__c','CanaryAMS__Salutation__c');
            wrapperobj.includedExcludedPickList = getPicklistValues('CanaryAMS__Insured__c','CanaryAMS__Included_Excluded__c');
            wrapperobj.relationshipPickList = getPicklistValues('CanaryAMS__Insured__c', 'CanaryAMS__Relationship__c');
            wrapperobj.maritalStatusPickList = getPicklistValues('CanaryAMS__Insured__c', 'CanaryAMS__Marital_Status__c');
            wrapperobj.genderPickList = getPicklistValues('CanaryAMS__Insured__c', 'CanaryAMS__Gender__c');
            wrapperobj.agricultureCoverageTypePicklist = getPicklistValues('CanaryAMS__Insurance_Product__c', 'CS_Agriculture_Coverage__c');
            wrapperobj.accountStatusTypePicklist = getPicklistValues('Account', 'CanaryAMS__Account_Status__c');
            wrapperobj.additionalPropertyPicklist = getPicklistValues('CanaryAMS__Insurance_Product__c', 'CS_Additional_property__c');
            System.debug('--------------wrapperobj--------'+wrapperobj);
            return wrapperobj;
        } catch (Exception ex) {
            System.debug('--Exception---Line Number------' + ex.getLineNumber());
            System.debug('--Exception---Line Number------' + ex.getMessage());
            return null;
        }
    }
    
    public static List<CreateQuoteUtilWrapper.PickListDetails> getPicklistValues(String ObjectApi_name,String Field_name) {
        try { 
            List <CreateQuoteUtilWrapper.PickListDetails> lstPickvals = new List <CreateQuoteUtilWrapper.PickListDetails>();
            Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectApi_name);//From the Object Api name retrieving the SObject
            Sobject Object_name = targetType.newSObject();
            Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
            Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
            Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
            List<Schema.PicklistEntry> pick_list_values = field_map.get(Field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
            for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
                if (a.isActive()) {
                    CreateQuoteUtilWrapper.PickListDetails pickListdetailobj = new CreateQuoteUtilWrapper.PickListDetails();
                    pickListdetailobj.label = a.getLabel();
                    pickListdetailobj.value = a.getValue();
                    lstPickvals.add(pickListdetailobj);//add the value  to our final list
                    //lstPickvals.add(new SelectOption( a.getValue(), a.getValue() ));
                }
            }
            return lstPickvals;
        } catch (Exception ex) {
            System.debug('--Exception---Line Number------' + ex.getLineNumber());
            System.debug('--Exception---Line Number------' + ex.getMessage());
            return null;
        }
    }
    
}