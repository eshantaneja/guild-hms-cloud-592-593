public class BuildQuoteController {
    
    @AuraEnabled
    public static CreateQuoteUtilWrapper.PickListWapper getAllPickListRecord() {
        try {
            return CreateQuoteUtilClass.getAllPickListRecord();
            
        } catch (Exception ex) {
            System.debug('--Exception---Line Number------' + ex.getLineNumber());
            System.debug('--Exception---Line Number------' + ex.getMessage());
            return null;
        }
    }
    
    @AuraEnabled
    public static String saveRecords_Apex(Account accountObj, CanaryAMS__Insurance_Product__c insuranceQuoteObj,
                                          List<CanaryAMS__Insured__c> insuredList, List<CanaryAMS__Commercial_Vehicle__c> vehicleList,
                                          List<CanaryAMS__Property__c> propertyList)
    {
        try {
            
            Id accountTypeId, quoteTypeId;
            
            if(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business') != NULL){
                accountTypeId =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business').getRecordTypeId();
            }
            
            if(insuranceQuoteObj.CS_Commercial_Coverage_Type__c == 'Commercial Property'){
                if(Schema.SObjectType.CanaryAMS__Insurance_Product__c.getRecordTypeInfosByName().get('Commercial Property') !=  null){
                    quoteTypeId = Schema.SObjectType.CanaryAMS__Insurance_Product__c.getRecordTypeInfosByName().get('Commercial Property').getRecordTypeId();
                }
            }
           if(insuranceQuoteObj.CS_Commercial_Coverage_Type__c == 'Commercial Auto'){
                if(Schema.SObjectType.CanaryAMS__Insurance_Product__c.getRecordTypeInfosByName().get('Commercial Auto') !=  null){
                    quoteTypeId = Schema.SObjectType.CanaryAMS__Insurance_Product__c.getRecordTypeInfosByName().get('Commercial Auto').getRecordTypeId();
                }
            }
            if(insuranceQuoteObj.CS_Agriculture_Coverage__c == 'Agriculture Property'){
                if(Schema.SObjectType.CanaryAMS__Insurance_Product__c.getRecordTypeInfosByName().get('Agriculture Property') !=  null){
                    quoteTypeId = Schema.SObjectType.CanaryAMS__Insurance_Product__c.getRecordTypeInfosByName().get('Agriculture Property').getRecordTypeId();
                }
            }
            if(insuranceQuoteObj.CS_Agriculture_Coverage__c == 'Agriculture Package'){
                if(Schema.SObjectType.CanaryAMS__Insurance_Product__c.getRecordTypeInfosByName().get('Agriculture Package') !=  null){
                    quoteTypeId = Schema.SObjectType.CanaryAMS__Insurance_Product__c.getRecordTypeInfosByName().get('Agriculture Package').getRecordTypeId();
                }
            }
            
            if(accountTypeId != null){
                accountObj.RecordTypeId = accountTypeId;
            }
            
            insert accountObj;
            
            insuranceQuoteObj.Name = accountObj.Name;
            insuranceQuoteObj.CanaryAMS__Account__c = accountObj.Id;
            
            if(quoteTypeId != null){
                insuranceQuoteObj.RecordTypeId = quoteTypeId;
            }
            insert insuranceQuoteObj;
            
            createInsuredRecords(insuredList, insuranceQuoteObj.Id, accountObj.Id);
            createPropertyRecords(propertyList, insuranceQuoteObj.Id, accountObj.Id);
            createVehicleRecords(vehicleList, insuranceQuoteObj.Id, accountObj.Id);
            
            return insuranceQuoteObj.Id;
            
        } catch (Exception ex) {
            System.debug('Exceptiion :: ' + ex.getMessage() + ' on line :: ' + ex.getLineNumber() + ' cause ::' + ex.getCause());
            return null;
        }
    }
    
    public static void createVehicleRecords(List<CanaryAMS__Commercial_Vehicle__c> vehicleList, Id insuranceQuoteId, Id accountId){
        try{
            if(vehicleList != null && vehicleList.size() > 0){
                /*Id vehicleTypeId;
                if(Schema.SObjectType.CanaryAMS__Commercial_Vehicle__c.getRecordTypeInfosByName().get('Commercial') != NULL){
                    vehicleTypeId =  Schema.SObjectType.CanaryAMS__Commercial_Vehicle__c.getRecordTypeInfosByName().get('Commercial').getRecordTypeId();
                }*/
                for(CanaryAMS__Commercial_Vehicle__c vehicleObj : vehicleList){
                    /*if(vehicleTypeId != null){
                        vehicleObj.RecordTypeId = vehicleTypeId;
                    }*/
                    vehicleObj.CanaryAMS__Insurance_Product__c = insuranceQuoteId;
                }
                insert vehicleList;
                System.debug('vehicleList :: ' + vehicleList);
            }
        } catch (Exception ex) {
            System.debug('Exceptiion :: ' + ex.getMessage() + ' on line :: ' + ex.getLineNumber() + ' cause ::' + ex.getCause());
        }
    }
    
    public static void createPropertyRecords(List<CanaryAMS__Property__c> propertyList, Id insuranceQuoteId, Id accountId){
        try{
            if(propertyList != null && propertyList.size() > 0){
                Id propertyTypeId;
                if(Schema.SObjectType.CanaryAMS__Property__c.getRecordTypeInfosByName().get('Commercial') != NULL){
                    propertyTypeId =  Schema.SObjectType.CanaryAMS__Property__c.getRecordTypeInfosByName().get('Commercial').getRecordTypeId();
                }
                for(CanaryAMS__Property__c propertyObj : propertyList){
                    if(propertyTypeId != null){
                        propertyObj.RecordTypeId = propertyTypeId;
                    }
                    propertyObj.CanaryAMS__Insurance_Product__c = insuranceQuoteId;
                }
                insert propertyList;
            }
        } catch(Exception ex) {
            System.debug('Exceptiion :: ' + ex.getMessage() + ' on line :: ' + ex.getLineNumber() + ' cause ::' + ex.getCause());
        }
    }
    
    public static void createInsuredRecords(List<CanaryAMS__Insured__c> insuredList, Id insuranceQuoteId, Id accountId){
        try{
            if(insuredList != null && insuredList.size() > 0){
                Id insuredTypeId;
                if(Schema.SObjectType.CanaryAMS__Insured__c.getRecordTypeInfosByName().get('Insured') != NULL){
                    insuredTypeId =  Schema.SObjectType.CanaryAMS__Insured__c.getRecordTypeInfosByName().get('Insured').getRecordTypeId();
                }
                for(CanaryAMS__Insured__c insuredObj : insuredList){
                    if(insuredTypeId != null){
                        insuredObj.RecordTypeId = insuredTypeId;
                    }
                    insuredObj.CanaryAMS__Product_Name__c = insuranceQuoteId;
                    insuredObj.CanaryAMS__Account__c = accountId;
                }
                insert insuredList;
            }
        } catch (Exception ex) {
            System.debug('Exceptiion :: ' + ex.getMessage() + ' on line :: ' + ex.getLineNumber() + ' cause ::' + ex.getCause());
        }
        
    }
    
}