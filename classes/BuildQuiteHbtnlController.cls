/*
 * Created by CA on 2/18/2019.
 */
public class BuildQuiteHbtnlController {
	@AuraEnabled
	public static CreateQuoteUtilWrapper.PickListWapper getAllPickListRecord() {
		try {
			return CreateQuoteUtilClass.getAllPickListRecord();
		} catch (Exception ex) {
			System.debug('--Exception---Line Number------' + ex.getLineNumber());
			System.debug('--Exception---Line Number------' + ex.getMessage());
			return null;
		}
	}
	@AuraEnabled
	public static  String createQuoteRecord(Account accountObj,CanaryAMS__Insurance_Product__c insuranceProduct,List<CanaryAMS__Insured__c>  insuredList,List<CanaryAMS__Property__c> propertyList){
		try {
			if(accountObj != null){
				System.debug('-----accountObj----'+accountObj);
				//insert accountObj;
			}
			System.debug('----aaaaaaaaaaaaaaa----'+Schema.getGlobalDescribe().get('CanaryAMS__Insurance_Product__c').getDescribe().getRecordTypeInfosByName().get('Habitational').getRecordTypeId());
            if(insuranceProduct != null){
				System.debug('-------insuranceProduct----'+insuranceProduct);
                insuranceProduct.Name = accountObj.Name+'Habitational';
				insuranceProduct.RecordTypeId = Schema.getGlobalDescribe().get('CanaryAMS__Insurance_Product__c').getDescribe().getRecordTypeInfosByName().get('Habitational').getRecordTypeId();
				//insert insuranceProduct;
				return 'success---'+insuranceProduct.Id;
			}

			return null;
		} catch (Exception ex) {
			System.debug('--Exception---Line Number------' + ex.getLineNumber());
			System.debug('--Exception---Line Number------' + ex.getMessage());
			return null;
		}

	}
}