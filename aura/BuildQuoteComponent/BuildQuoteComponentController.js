({
    doInit : function(c, e, h) {
        c.set("v.insuranceQuoteObj.CanaryAMS__Quoted_Term__c", "12 Month");
        h.setPicklistValues(c, e, h);
        h.setDefaultBirthDate(c, e, h);
    },
    
    gotoNext : function(c, e, h){
        h.gotoNext_Helper(c, e, h);
    },
    
    gotoPrevious : function(c, e, h){
        h.gotoPrevious_Helper(c, e, h);
    },
    
    addAnotherInsured : function(c, e, h){
        let getChoice = e.getSource().get("v.value");
        c.set("v.insuranceQuoteObj.CS_Additional_Insured__c", getChoice);
        if(getChoice == "Yes"){
            c.set("v.insuredObj", {'sobjectType' : 'CanaryAMS__Insured__c'});
            c.set("v.screenNumber", 3);
            e.getSource().set("v.value","");
        }
        if(getChoice == "No"){
            h.gotoNext_Helper(c, e, h);
        }
    },
    
    copyShippingAddress : function(c, e, h){
        let isChecked = e.getSource().get("v.checked");
        if(isChecked){
            let getAccountObj = c.get("v.accountObj");
            getAccountObj.BillingStreet = getAccountObj.ShippingStreet;
            getAccountObj.BillingCity = getAccountObj.ShippingCity;
            getAccountObj.BillingStateCode = getAccountObj.ShippingStateCode;
            getAccountObj.BillingPostalCode = getAccountObj.ShippingPostalCode;
            getAccountObj.BillingCountry = getAccountObj.ShippingCountry;
            c.set("v.accountObj", getAccountObj);
        }
    },
    
    setAccountType : function(c, e, h){
        let getQuoteCoverageType= e.getSource().get("v.value");
        
        if(c.get("v.openNewWindow") == "Agriculture"){
            c.set("v.insuranceQuoteObj.CS_Agriculture_Coverage__c", getQuoteCoverageType);
        } else{
            c.set("v.insuranceQuoteObj.CS_Commercial_Coverage_Type__c", getQuoteCoverageType);
        }
        
        if(getQuoteCoverageType == "Commercial Auto" || getQuoteCoverageType == "Commercial Property"
          || getQuoteCoverageType == "Agriculture Property" || getQuoteCoverageType == "Agriculture Package"){
            
            if(getQuoteCoverageType == "Commercial Auto" || getQuoteCoverageType == "Agriculture Package"){
                c.set("v.propertyObj", {'sobjectType' : 'CanaryAMS__Property__c'});
                c.set("v.propertyObjList", []);
            } else{
                c.set("v.vehicleObj", {'sobjectType' : 'CanaryAMS__Commercial_Vehicle__c'});
                c.set("v.vehicleObjList", []);
            }
            c.set("v.accountRecordType", "Business");
        }
        
    },
    
    getPropertyChoice : function(c, e, h){
        let getChoice = e.getSource().get("v.value");
        c.set("v.insuranceQuoteObj.CS_Mailing_is_Property__c", getChoice);
        c.set("v.prevPropertyScreenNumber", c.get("v.propertyScreenNumber"));
        if(getChoice == "Yes"){
            h.copyAccountMailingAddressToProperty(c, e, h);
            e.getSource().set("v.value","");
            c.set("v.fromPropertyScreen1", true);
            c.set("v.propertyScreenNumber", 3);
        }
        if(getChoice == "No"){
            c.set("v.propertyScreenNumber", 2);
            e.getSource().set("v.value","");
        }
    },
    
    addAnotherPropertyChoice : function(c, e, h){
        let getChoice = e.getSource().get("v.value");
        c.set("v.insuranceQuoteObj.CS_Additional_property__c", getChoice);
        if(getChoice == "Yes"){
            c.set("v.prevPropertyScreenNumber", 3);
            c.set("v.propertyScreenNumber", 2);
            c.set("v.propertyObj", {'sobject' : 'CanaryAMS__Property__c'});
            e.getSource().set("v.value","");
        }
        if(getChoice == "No"){
            h.saveAllRecords_Helper(c, e, h);
            e.getSource().set("v.value","");
        }
    },
    
    addAnotherVehicle : function(c, e, h){
        let getChoice = e.getSource().get("v.value");
        c.set("v.insuranceQuoteObj.CS_Additional_Vehicle__c", getChoice);
        if(getChoice == "Yes"){
            c.set("v.vehicleScreenNumber", 1);
            e.getSource().set("v.value","");
            c.set("v.vehicleObj", {'sobjectType' : 'CanaryAMS__Commercial_Vehicle__c'});
        } else if(getChoice == "No"){
            h.saveAllRecords_Helper(c, e, h);
            e.getSource().set("v.value","");
        }
    },
    
    saveAllRecords : function(c, e, h){
        h.saveAllRecords_Helper(c, e, h);
    },
})