({
    callServer : function(component, method, callback, params) {//here method is of an apex class
        try{
            var action = component.get(method);
            if(params){
                action.setParams(params);
            }
            action.setCallback(this,function(response){
                var state = response.getState();
                if(state === "SUCCESS"){
                    callback.call(this,response.getReturnValue());
                } else if(state === "ERROR"){
                    if(component.get("v.showSpinner")){
                        component.set("v.showSpinner", false);
                    }
                    var errors = response.getError();
                    if(errors){
                        console.log("Errors",errors);
                        if(errors[0] && errors[0].message){
                            throw new Error("Error" + errors[0].message);
                        }
                    } else{
                        throw new Error("Unknown Error");
                    }
                }
            });
            $A.enqueueAction(action);
        }
        catch(ex){
            console.log('Message :: ' + ex.message);
        }
    },
    
    doInitHelper : function(c, e, h) {
        try{
            
        }
        catch(ex){
            console.log('Message :: ' + ex.message);
        }
    },
    
    gotoNext_Helper : function(c, e, h){
        let getScreenNumber = c.get("v.screenNumber");
        
        switch(getScreenNumber){
            case 1 : let isValidQuote = h.validateFirstScreenInput(c, e, h);
                if(! isValidQuote){
                    return;
                }
                getScreenNumber++;
                break;
            case 2 : let isValidAccount = h.validateAccountDetails(c, e, h);
                if(isValidAccount){
                    getScreenNumber++;
                }
                break;
            case 3: let isValidInsured = h.validateInsuredDetails(c, e, h);
                if(isValidInsured){
                    let insuredList = c.get("v.insuredObjList");
                    insuredList.push(c.get("v.insuredObj"));
                    c.set("v.insuredObjList", insuredList);
                    getScreenNumber++;
                }
                break;
            case 4 : 
                if(c.get("v.insuranceQuoteObj.CS_Commercial_Coverage_Type__c") == "Commercial Property"
                  || c.get("v.insuranceQuoteObj.CS_Agriculture_Coverage__c") == "Agriculture Property"){
                    c.set("v.propertyScreenNumber", 1);
                } else{
                    c.set("v.vehicleScreenNumber", 1);
                }
                getScreenNumber++;
                break;
                
            case 5 : 
                if(c.get("v.insuranceQuoteObj.CS_Commercial_Coverage_Type__c") == "Commercial Property"
                  || c.get("v.insuranceQuoteObj.CS_Agriculture_Coverage__c") == "Agriculture Property"){
                    if(c.get("v.propertyScreenNumber") == 2){
                        let isProprtyValid = h.validatePropertyRecord(c, e, h);
                        if(isProprtyValid){
                            let getPropertyList = c.get("v.propertyObjList");
                            getPropertyList.push(c.get("v.propertyObj"));
                            c.set("v.propertyObjList", getPropertyList);
                            c.set("v.propertyScreenNumber", 3);
                        }
                        
                    }
                } else{
                    if(c.get("v.vehicleScreenNumber") == 0){
                        c.set("v.vehicleScreenNumber", 1);
                    } else if(c.get("v.vehicleScreenNumber") == 1){
                        let isValidVehicles = h.validateVehicles(c, e, h);
                        if(isValidVehicles){
                            let getVehcileList = c.get("v.vehicleObjList");
                            getVehcileList.push(c.get("v.vehicleObj"));
                            c.set("v.vehicleObjList", getVehcileList);
                            c.set("v.vehicleScreenNumber", 2);
                        }
                        
                    }
                    
                }
                
                break;
                
        }
        c.set("v.screenNumber", getScreenNumber);
        
    },
    
    gotoPrevious_Helper : function(c, e, h){
        let getScreenNumber = c.get("v.screenNumber");
        
        switch(getScreenNumber){
            case 1: c.set("v.openNewWindow",'none');
                break;
            case 2 : getScreenNumber--;
                break;
            case 3 : getScreenNumber--;
                break;
            case 4 : getScreenNumber--;
                break;
            case 5 : 
                if(c.get("v.insuranceQuoteObj.CS_Commercial_Coverage_Type__c") == "Commercial Property"
                  || c.get("v.insuranceQuoteObj.CS_Agriculture_Coverage__c") == "Agriculture Property"){
                    if(c.get("v.propertyScreenNumber") == 1){
                        c.set("v.propertyScreenNumber", 0);
                        getScreenNumber--;
                    } else if(c.get("v.propertyScreenNumber") == 2){
                        c.set("v.propertyScreenNumber", c.get("v.prevPropertyScreenNumber"));
                    } else if(c.get("v.propertyScreenNumber") == 3){
                        c.set("v.propertyScreenNumber", 1);
                    }
                } else{
                    if(c.get("v.vehicleScreenNumber") == 1){
                        c.set("v.vehicleScreenNumber", 0);
                        getScreenNumber--;
                    } else if(c.get("v.vehicleScreenNumber") == 2){
                        c.set("v.vehicleScreenNumber", 1);
                    }
                }
                
                break;
        }
        c.set("v.screenNumber", getScreenNumber);
    },
    
    saveAllRecords_Helper : function(c, e, h){
        let accountObj = c.get("v.accountObj");
        let quoteObj = c.get("v.insuranceQuoteObj");
        let insuredObjList = c.get("v.insuredObjList");
        let propertyObjList = c.get("v.propertyObjList");
        let vehicleList = c.get("v.vehicleObjList");
        h.toggleSpinner(c, e, h);
        h.callServer(c, "c.saveRecords_Apex",
                     function(response){
                         h.toggleSpinner(c, e, h);
                         if(response != null){
                             c.set("v.openNewWindow",'none');
                             h.navigateToDetailPage(c, e, h, response);
                             var utilityAPI = c.find("utilitybar");
                             utilityAPI.minimizeUtility();
                         } else{
                             h.showInfoToast(c, e, h, 'Try Again', 'error', 'dismissible', 'Error');
                             return;
                         }
                     },
                     {accountObj : accountObj,
                      insuranceQuoteObj : quoteObj,
                      insuredList : insuredObjList,
                      vehicleList : vehicleList,
                      propertyList : propertyObjList});
    },
    
    validateVehicles : function(c, e, h){
        let vehicleObj = c.get("v.vehicleObj");
        if(vehicleObj.Name != undefined && vehicleObj.Name != ""){
            if(vehicleObj.Name.trim() != ""){
                return true;
            }
        } else{
            h.showInfoToast(c, e, h, 'Please review your Inputs', 'error', 'dismissible', 'Error');
            return false;
        }
    },
    
    validatePropertyRecord : function(c, e, h){
        let propertyObj = c.get("v.propertyObj");
        if(propertyObj.Name != undefined && propertyObj.Name != ""){
            if(propertyObj.Name.trim() != ""){
                return true;
            }
        } else{
            h.showInfoToast(c, e, h, 'Please review your Inputs', 'error', 'dismissible', 'Error');
            return false;
        }
    },
    
    validateInsuredDetails : function(c, e, h){
        let allValid  = c.find('insured-1').reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);
        
        if(! allValid){
            h.showInfoToast(c, e, h, 'Please review your Inputs', 'error', 'dismissible', 'Error');
            return allValid;
        }
        
        let getInsuredPhone = c.get("v.insuredObj.CanaryAMS__Phone__c");
        if(! h.validatePhoneNumber(getInsuredPhone)){
            h.showInfoToast(c, e, h, 'Please review Phone Number', 'error', 'dismissible', 'Error');
            return false;
        }
        return true;
    },
    
    validateAccountDetails : function(c, e, h){
        let allValid  = c.find('account-1').reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);
        
        if(! allValid){
            h.showInfoToast(c, e, h, 'Please review your Inputs', 'error', 'dismissible', 'Error');
            return allValid;
        }
        
        let getAccountMobilePhone = c.get("v.accountObj.TC_Cell_Phone__c");
        let getAccountFax = c.get("v.accountObj.Fax");
        
        if(! h.validatePhoneNumber(getAccountMobilePhone)){
            h.showInfoToast(c, e, h, 'Please review Mobile Phone', 'error', 'dismissible', 'Error');
            return false;
        }
        if(! h.validatePhoneNumber(getAccountMobilePhone)){
            h.showInfoToast(c, e, h, 'Please review Fax', 'error', 'dismissible', 'Error');
            return false;
        }
        return true;
    },
    
    validatePhoneNumber : function(phoneNumber){
        if(phoneNumber != "" && phoneNumber != undefined && phoneNumber.trim() != ""){
            if(isNaN(phoneNumber.replace(/\s/g,'').replace('(','').replace(')','').replace('-','')) || phoneNumber.length != 10){
                return false;
            }
        }
        return true;
    },
    
    copyAccountMailingAddressToProperty : function(c, e, h){
        let accountObj = c.get("v.accountObj");
        if(accountObj.ShippingStreet != undefined ){
            let propertyList = [];
            let propertyObj = {'sobjectType' : 'CanaryAMS__Property__c'};
            propertyObj.Name = accountObj.ShippingStreet;
            propertyObj.CanaryAMS__City__c = accountObj.ShippingCity;
            propertyObj.CanaryAMS__State__c = accountObj.ShippingStateCode;
            propertyObj.CanaryAMS__Zip_Code__c = accountObj.ShippingPostalCode;
            propertyList.push(propertyObj);
            c.set("v.propertyObjList", propertyList);
        }
        
    },
    
    validateFirstScreenInput : function(c, e, h){
        return c.find('element_1').reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);
    },
    
    setPicklistValues : function(c, e, h){
        h.toggleSpinner(c, e, h);
        h.callServer(c, "c.getAllPickListRecord",
                     function(response){
                         h.toggleSpinner(c, e, h);
                         if(response != null){
                             if(c.get("v.openNewWindow") == "Agriculture"){
                                 if(response.agricultureCoverageTypePicklist){
                                     c.set("v.quoteRecordType", response.agricultureCoverageTypePicklist);
                                 }
                             } else{
                                 if(response.commercialCoverageTypePicklist){
                                     c.set("v.quoteRecordType", response.commercialCoverageTypePicklist);
                                 }
                             }
                             
                             if(response.quotedTermPicklist){
                                 c.set("v.quoteTermLength", response.quotedTermPicklist);
                             }
                             if(response.accountStatusTypePicklist){
                                 c.set("v.accountStatus", response.accountStatusTypePicklist);
                             }
                             if(response.salutationPickList){
                                 c.set("v.insuredSalutations", response.salutationPickList);
                             }
                             if(response.applicantPickList){
                                 c.set("v.insuredApplicants", response.applicantPickList);
                             }
                             if(response.includedExcludedPickList){
                                 c.set("v.insuredIncludedExcluded", response.includedExcludedPickList);
                             }  
                             if(response.relationshipPickList){
                                 c.set("v.insuredRelationship", response.relationshipPickList);
                             }
                             if(response.maritalStatusPickList){
                                 c.set("v.insuredMarital", response.maritalStatusPickList);
                             }
                             if(response.genderPickList){
                                 c.set("v.insuredGender", response.genderPickList);
                             }
                             if(response.additionalInsuredPickList){
                                 c.set("v.addAnotherInsured", response.additionalInsuredPickList);
                             }
                             if(response.mailingIsPropertyPickList){
                                 c.set("v.isMailingProperty", response.mailingIsPropertyPickList);
                             }
                             if(response.additionalPropertyPicklist){
                                 c.set("v.addAnotherProperty", response.additionalPropertyPicklist);
                             }
                         }
                     },
                     {/*recId : "12365"*/});
    },
    
    setDefaultBirthDate : function(c, e, h){
        let today;
        var setDate = new Date();
        var dd = setDate.getDate();
        var mm = setDate.getMonth()+1; //January is 0!
        var yyyy = setDate.getFullYear();
        if(dd<10) {
            dd = '0'+dd
        }
        
        if(mm<10) {
            mm = '0'+mm
        }
        
        today = yyyy + '-' + mm + '-' + dd;
        
        c.set("v.maxBirthDate", today);
        
    },
    
    showInfoToast: function (component, event, helper, message, type, mode, title)  {
        var toastEvent = $A.get("event.force:showToast");
        if(toastEvent){
            toastEvent.setParams({
                title: title,
                message: message,
                duration: 2000,
                key: "info_alt",
                type: type,
                mode: mode
            });
            toastEvent.fire();
        } else{
            alert(message);
        }
    },
    
    toggleSpinner : function(c, e, h){
        var isShown = c.get("v.showSpinner");
        if(isShown){
            isShown = false;
        } else{
            isShown = true;
        }
        c.set("v.showSpinner", isShown);
    },
    
    navigateToDetailPage : function(c, e, h, recordId){
        var navEvt = $A.get("e.force:navigateToSObject");
        if(navEvt){
            navEvt.setParams({
                "recordId" : recordId,
                "slideDevName" : "detail"
            });
            navEvt.fire();
        }
    },
    
})