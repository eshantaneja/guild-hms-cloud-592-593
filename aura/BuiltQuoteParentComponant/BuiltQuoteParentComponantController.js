({
     doInit: function(c,e,h){
             var action = c.get("c.getAllPickListRecord");
             action.setParams({

             });
             action.setCallback(this,function(response){
             console.log('-paernt---response.getState()------'+response.getState());
                 if(response.getState() === "SUCCESS"){
                     var storedResponse = response.getReturnValue();
                     if( storedResponse != undefined && storedResponse != null && storedResponse != '' ){
                         c.set("v.allPickListRecord",storedResponse);
                         c.set("v.isOpenSpiner",false);
                     }
                 }
             });
             $A.enqueueAction(action);
     },
     firstScreenNext: function(c,e,h){
       var openNewWindow = c.get("v.openNewWindow");
       var insuranceProduct = c.get("v.insuranceProduct");
       if(insuranceProduct.Quote_Record_Type__c != undefined && insuranceProduct.Quote_Record_Type__c != '' && insuranceProduct.Quote_Record_Type__c != null  && insuranceProduct.Quote_Record_Type__c != 'none'){
           openNewWindow = insuranceProduct.Quote_Record_Type__c;
           c.set("v.openNewWindow",openNewWindow);
       }else{
            var message = 'Please select the  quote record Type';
            h.showInfoToast(c, e, h, 'Error', message, 'info_alt', 'error', 'dismissible');
            return;
       }
     },
})