({
	helperMethod : function() {
		
	},
	showInfoToast: function (component, event, helper, title, message, key, type, mode)  {
            var toastEvent = $A.get("event.force:showToast");
            toastEvent.setParams({
                title: title,
                message: message,
                duration: 2000,
                key: key,
                type: type,
                mode: mode
            });
            toastEvent.fire();
        },
})