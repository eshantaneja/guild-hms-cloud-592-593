({
    doInit_helper : function (c, e, h){
        var isOpenScreen = {
            SecondScreen:true,
            ThirdScreen:false,
            FourthScreen:false,
            FifthScreen:false,
            SixthScreen:false,
            SeventhScreen:false,
            EightsScreen:false
        };
        c.set("v.isOpenScreen",isOpenScreen);
    },
    
    secondScreenNext_helper: function (c, e, h){
        var isOpenScreen =  c.get("v.isOpenScreen");
        isOpenScreen.SecondScreen = false;
        isOpenScreen.ThirdScreen = true;
        c.set("v.isOpenScreen",isOpenScreen);
    },
    thirdScreenNextValidation_helper : function(c,e,h){
        var accountObj = c.get("v.accountObj");
        if(!$A.util.isEmpty(accountObj) && accountObj != undefined){
            if(accountObj.Name != undefined && !$A.util.isEmpty(accountObj.Name)){
                accountObj.Name = (accountObj.Name).trim();
            }
            if(accountObj.Name == undefined || $A.util.isEmpty(accountObj.Name)){
                var message = 'Please enter the account name';
                h.showInfoToast(c, e, h, 'Required', message, 'info_alt', 'error', 'dismissible');
                return;
            }
            if(accountObj.TC_Email__c != undefined && !$A.util.isEmpty(accountObj.TC_Email__c)){
                accountObj.TC_Email__c = (accountObj.TC_Email__c).trim();
                if(!h.validateEmail(c,e,h,accountObj.TC_Email__c)){
                    var message = 'Please enter the valid email';
                    h.showInfoToast(c, e, h, 'Warning', message, 'info_alt', 'error', 'dismissible');
                    return;
                }
            }
            if(accountObj.BillingAddress != undefined && !$A.util.isEmpty(accountObj.BillingAddress)){
                accountObj.BillingAddress = (accountObj.BillingAddress).trim();
            }
            if(accountObj.BillingAddress == undefined || $A.util.isEmpty(accountObj.BillingAddress)){
                var message = 'Please enter the mailing address street';
                h.showInfoToast(c, e, h, 'Required', message, 'info_alt', 'error', 'dismissible');
                return;
            }
        }
        h.thirdScreenNext_helper(c, e, h);
    },
    cpoyBillingToMailing_helper: function (c, e, h){
        var accountObj = c.get("v.accountObj");
        if(!$A.util.isEmpty(accountObj) && accountObj != undefined){
            if(!$A.util.isEmpty(accountObj.CS_Use_Mailing_Address)  && accountObj.CS_Use_Mailing_Address != undefined && accountObj.CS_Use_Mailing_Address == true){
                if(!$A.util.isEmpty(accountObj.BillingAddress)  && accountObj.BillingAddress != undefined){
                    accountObj.ShippingAddress = accountObj.BillingAddress;
                }if(!$A.util.isEmpty(accountObj.BillingCity)  && accountObj.BillingCity != undefined){
                    accountObj.ShippingCity = accountObj.BillingCity;
                }if(!$A.util.isEmpty(accountObj.BillingState)  && accountObj.BillingState != undefined){
                    accountObj.ShippingState = accountObj.BillingState;
                }if(!$A.util.isEmpty(accountObj.BillingPostalCode)  && accountObj.BillingPostalCode != undefined){
                    accountObj.ShippingPostalCode = accountObj.BillingPostalCode;
                }if(!$A.util.isEmpty(accountObj.BillingCountry)  && accountObj.BillingCountry != undefined){
                    accountObj.ShippingCountry = accountObj.BillingCountry;
                }
            }
        }
        c.set("v.accountObj",accountObj);
    },
    thirdScreenNext_helper: function (c, e, h){
        var isOpenScreen =  c.get("v.isOpenScreen");
        isOpenScreen.ThirdScreen = false;
        isOpenScreen.FourthScreen = true;
        c.set("v.isOpenScreen",isOpenScreen);
    },
    fourthScreenNextValidation_helper : function (c,e,h){
        var insuredObj = c.get("v.insuredObj");
        if(!$A.util.isEmpty(insuredObj) && insuredObj != undefined){
           if(insuredObj.Name != undefined && !$A.util.isEmpty(insuredObj.Name)){
               insuredObj.Name = (insuredObj.Name).trim();
           }
           if(insuredObj.Name == undefined || $A.util.isEmpty(insuredObj.Name)){
               var message = 'Please enter the insured name';
               h.showInfoToast(c, e, h, 'Required', message, 'info_alt', 'error', 'dismissible');
               return;
           }
            if(insuredObj.CanaryAMS__Email__c != undefined && !$A.util.isEmpty(insuredObj.CanaryAMS__Email__c)){
                insuredObj.CanaryAMS__Email__c = (insuredObj.CanaryAMS__Email__c).trim();
                if(!h.validateEmail(c,e,h,insuredObj.CanaryAMS__Email__c)){
                    var message = 'Please enter the valid email';
                    h.showInfoToast(c, e, h, 'Warning', message, 'info_alt', 'error', 'dismissible');
                    return;
                }
            }if(insuredObj.CanaryAMS__Date_of_Birth__c != undefined && !$A.util.isEmpty(insuredObj.CanaryAMS__Date_of_Birth__c)){
                var dateOfBirth  = h.validateDate(c,e,h,insuredObj.CanaryAMS__Date_of_Birth__c);
                if(h.validateDateOfBirth(c,e,h,dateOfBirth)){
                    var today = new Date();
                    var StrList = dateOfBirth.split('/');
                    var date2 = new Date(StrList[2], parseInt(StrList[1], 10)-1, StrList[0], 0, 0, 0, 0);
                    if(date2.getTime() > today.getTime()){
                        message = 'You entered future date of birth';
                        h.showInfoToast(c, e, h, 'Warning', message, 'info_alt', 'error', 'dismissible');
                        return;
                    }
                }else{
                    var message = 'Please enter the valid date of birth';
                    h.showInfoToast(c, e, h, 'Warning', message, 'info_alt', 'error', 'dismissible');
                    return;
                }
            }
        }
        h.fourthScreenNext_helper(c,e,h);
    },
    fourthScreenNext_helper: function (c, e, h){
        var isOpenScreen =  c.get("v.isOpenScreen");
        isOpenScreen.FourthScreen = false;
        isOpenScreen.FifthScreen = true;
        c.set("v.isOpenScreen",isOpenScreen);
    },
    fifthScreenNext_helper: function (c, e, h){
        var isOpenScreen =  c.get("v.isOpenScreen");
        isOpenScreen.FifthScreen = false;
        isOpenScreen.SixthScreen = true;
        c.set("v.isOpenScreen",isOpenScreen);
    },
    sixthScreenNext_helper: function (c, e, h){
        var insuranceProduct = c.get("v.insuranceProduct");
        var accountObj = c.get("v.accountObj");
        var propertyObj = {'sobjectType' : 'CanaryAMS__Property__c'};
        var propertyList = c.get("v.propertyList");
        var selectedValue  = e.getSource().get("v.label");
        if(selectedValue == 'Yes'){
            if(!$A.util.isEmpty(accountObj) && accountObj != undefined){
                if(!$A.util.isEmpty(accountObj.BillingAddress)  && accountObj.BillingAddress != undefined){
                    propertyObj.Name = accountObj.BillingAddress;
                }if(!$A.util.isEmpty(accountObj.BillingCity)  && accountObj.BillingCity != undefined){
                    propertyObj.CanaryAMS__City__c = accountObj.BillingCity;
                }if(!$A.util.isEmpty(accountObj.BillingState)  && accountObj.BillingState != undefined){
                    propertyObj.CanaryAMS__State__c = accountObj.BillingState;
                }if(!$A.util.isEmpty(accountObj.BillingPostalCode)  && accountObj.BillingPostalCode != undefined){
                    propertyObj.CanaryAMS__Zip_Code__c = accountObj.BillingPostalCode;
                }if(!$A.util.isEmpty(accountObj.BillingCountry)  && accountObj.BillingCountry != undefined){
                    propertyObj.CanaryAMS__County__c = accountObj.BillingCountry;
                }
            }
             if(!$A.util.isEmpty(propertyObj.Name)  && propertyObj.Name != undefined){
               propertyList.push(propertyObj);
               c.set("v.propertyList",propertyList);
             }
            insuranceProduct.CS_Additional_property__c = selectedValue;
        }else if(selectedValue == 'No'){
            insuranceProduct.CS_Mailing_is_Property__c = selectedValue;
        }
        c.set("v.insuranceProduct",insuranceProduct);
        var isOpenScreen =  c.get("v.isOpenScreen");
        isOpenScreen.SixthScreen = false;
        isOpenScreen.SeventhScreen = true;
        c.set("v.isOpenScreen",isOpenScreen);
    },
    seventhScreenNext_helper: function (c, e, h){
        var propertyObj = c.get("v.propertyObj");
        if(!$A.util.isEmpty(propertyObj) && propertyObj != undefined){
            if(!$A.util.isEmpty(propertyObj.Name) && propertyObj.Name != undefined){
                propertyObj.Name = propertyObj.Name.trim();
            }
            if($A.util.isEmpty(propertyObj.Name) || propertyObj.Name == undefined){
                var message = 'Please enter the property address';
                h.showInfoToast(c, e, h, 'Required', message, 'info_alt', 'error', 'dismissible');
                return;
            }
        }
        var isOpenScreen =  c.get("v.isOpenScreen");
        isOpenScreen.SeventhScreen = false;
        isOpenScreen.EightsScreen = true;
        c.set("v.isOpenScreen",isOpenScreen);
    },
    secondScreenBack_helper: function (c, e, h){
        c.set("v.openNewWindow",'none');
    },
    thirdScreenBack_helper: function (c, e, h){
        var isOpenScreen =  c.get("v.isOpenScreen");
        isOpenScreen.ThirdScreen = false;
        isOpenScreen.SecondScreen = true;
        c.set("v.isOpenScreen",isOpenScreen);
    },
    fourthScreenBack_helper: function (c, e, h){
        var isOpenScreen =  c.get("v.isOpenScreen");
        isOpenScreen.FourthScreen = false;
        isOpenScreen.ThirdScreen = true;
        c.set("v.isOpenScreen",isOpenScreen);
    },
    fifthScreenBack_helper: function (c, e, h){
        var isOpenScreen =  c.get("v.isOpenScreen");
        isOpenScreen.FifthScreen = false;
        isOpenScreen.FourthScreen = true;
        c.set("v.isOpenScreen",isOpenScreen);
    },
    seventhScreenBack_helper: function (c, e, h){
        var isOpenScreen =  c.get("v.isOpenScreen");
        isOpenScreen.SixthScreen = false;
        isOpenScreen.SeventhScreen = false;
        isOpenScreen.FifthScreen = true;
        c.set("v.isOpenScreen",isOpenScreen);
    },
    eightsScreenBack_helper: function (c, e, h){
        var isOpenScreen =  c.get("v.isOpenScreen");
        isOpenScreen.EightsScreen = false;
        isOpenScreen.SeventhScreen = true;
        c.set("v.isOpenScreen",isOpenScreen);
    },
    addAdditionalPropertyEight_helper: function (c, e, h){
        var selectedValue  = e.getSource().get("v.label");
        var insuranceProduct  = c.get("v.insuranceProduct");
        var propertyObj = c.get("v.propertyObj");
        var propertyList = c.get("v.propertyList");
        propertyList.push(propertyObj);
        c.set("v.propertyList",propertyList);
        propertyObj = {'sobjectType' : 'CanaryAMS__Property__c'};
        insuranceProduct.CS_Additional_property__c = selectedValue;
        c.set("v.propertyObj",propertyObj);
        c.set("v.insuranceProduct",insuranceProduct);
        h.eightsScreenBack_helper(c,e,h);
    },
    addAnotherInsuredFifth_helper: function (c, e, h){
        var selectedValue  = e.getSource().get("v.label");
        var insuranceProduct  = c.get("v.insuranceProduct");
        var insuredObj = c.get("v.insuredObj");
        var insuredList = c.get("v.insuredList");
        insuredList.push(insuredObj);
        c.set("v.insuredList",insuredList);
        insuredObj = {'sobjectType' : 'CanaryAMS__Insured__c'};
        insuranceProduct.CS_Additional_Insured__c = selectedValue;
        c.set("v.insuredObj",insuredObj);
        c.set("v.insuranceProduct",insuranceProduct);
        h.fifthScreenBack_helper(c, e, h);
    },
    saveQuoteRecord_helper: function (c, e, h){
      var accountObj = c.get("v.accountObj");
      var insuranceProduct = c.get("v.insuranceProduct");
      var insuredObj = c.get("v.insuredObj");
      var insuredList = c.get("v.insuredList");
      var propertyObj = c.get("v.propertyObj");
      var propertyList = c.get("v.propertyList");
      if(!$A.util.isEmpty(propertyObj) && propertyObj != undefined){
          if(!$A.util.isEmpty(propertyObj.Name) && propertyObj.Name != undefined){
              propertyList.push(propertyObj);
              propertyObj = {'sobjectType' : 'CanaryAMS__Property__c'};
              c.set("v.propertyObj",propertyObj);
          }
      }if(!$A.util.isEmpty(insuredObj) && insuredObj != undefined){
         if(!$A.util.isEmpty(insuredObj.Name) && insuredObj.Name != undefined){
             insuredList.push(insuredObj);
             insuredObj ={'sobjectType' : 'CanaryAMS__Insured__c'};
             c.set("v.insuredObj",insuredObj);
         }
      }
      console.log('---accountObj--------'+JSON.stringify(accountObj));
      console.log('---insuranceProduct--------'+JSON.stringify(insuranceProduct));
      console.log('---insuredList--------'+JSON.stringify(insuredList));
      console.log('---propertyList--------'+JSON.stringify(propertyList));

      var action = c.get("c.createQuoteRecord");
           action.setParams({
               accountObj : accountObj,
               insuranceProduct : insuranceProduct,
               insuredList : insuredList,
               propertyList : propertyList
           });
           action.setCallback(this,function(response){
           console.log('-paernt---response.getState()------'+response.getState());
               if(response.getState() === "SUCCESS"){
                    var storedResponse = response.getReturnValue();
                    var message = 'Quote Record Created Successfully';
                    h.showInfoToast(c, e, h, 'Quote Saved', message, 'info_alt', 'success', 'dismissible');
                    var isOpenScreen =  c.get("v.isOpenScreen");
                    isOpenScreen.EightsScreen = false;
                    c.set("v.isOpenScreen",isOpenScreen);
                    c.set("v.openNewWindow",'none');
                    var utilityAPI = c.find("utilitybar");
                    utilityAPI.minimizeUtility();
                   console.log('---storedResponse--------'+storedResponse);
               }else{

               }
           });
           $A.enqueueAction(action);
    },

    showInfoToast: function (component, event, helper, title, message, key, type, mode)  {
        var toastEvent = $A.get("event.force:showToast");
        toastEvent.setParams({
            title: title,
            message: message,
            duration: 2000,
            key: key,
            type: type,
            mode: mode
        });
        toastEvent.fire();
    },
    validateEmail:function(c,e,h,message){
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (reg.test(message) == false){
            return false;
        }
        return true;
    },
    validateDate: function (c, e, h, testdate) {
        var bits = testdate.split('-');
        var dd = bits[2];
        var mm = bits[1];
        var yyyy = bits[0];
        if(dd != null && dd != undefined) {
            if(!dd.includes("0") && dd.length == 1){
                dd = '0'+dd;
            }
        }
        if(mm != null && mm != undefined) {
            if(!mm.includes("0") && mm.length == 1){
                mm = '0'+mm;
            }
        }
        var effDate  = dd+'/'+mm+'/'+yyyy; //chenge the formate
        return effDate;
    },
    validateDateOfBirth : function (c, e, h, testdate) {
        var date_regex =  /^\s*(3[01]|[12][0-9]|0?[1-9])\/(1[012]|0?[1-9])\/((?:19|20)\d{2})\s*$/ ;//dd/mm/yyy
        // var date_regex = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/ ;
        return date_regex.test(testdate);
    },

})