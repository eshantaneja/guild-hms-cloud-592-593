({
    doInit : function (c, e, h){
        h.doInit_helper(c, e, h);
    },
    secondScreenNext: function (c, e, h){
        h.secondScreenNext_helper(c, e, h);
    },
    thirdScreenNext: function (c, e, h){
        h.thirdScreenNextValidation_helper(c,e,h);
    },
    fourthScreenNext: function (c, e, h){
        h.fourthScreenNextValidation_helper(c, e, h);
    },
    fifthScreenNext: function (c, e, h){
          var selectedValue  = e.getSource().get("v.label");
          if(selectedValue == 'No'){
              var insuranceProduct = c.get("v.insuranceProduct");
              insuranceProduct.CS_Additional_Insured__c = selectedValue;
              c.set("v.insuranceProduct",insuranceProduct);
          }
          h.fifthScreenNext_helper(c, e, h);
    },
    sixthScreenNext: function (c, e, h){
        h.sixthScreenNext_helper(c, e, h);
    },
    seventhScreenNext: function (c, e, h){
        h.seventhScreenNext_helper(c, e, h);
    },
    secondScreenBack: function (c, e, h){
        h.secondScreenBack_helper(c, e, h);
    },
    thirdScreenBack: function (c, e, h){
        h.thirdScreenBack_helper(c, e, h);
    },
    fourthScreenBack: function (c, e, h){
        h.fourthScreenBack_helper(c, e, h);
    },
    fifthScreenBack: function (c, e, h){
        h.fifthScreenBack_helper(c, e, h);
    },
    seventhScreenBack: function (c, e, h){
        h.seventhScreenBack_helper(c, e, h);
    },
    eightsScreenBack: function (c, e, h){
        h.eightsScreenBack_helper(c, e, h);
    },
    addAdditionalPropertyEight: function (c, e, h){
        h.addAdditionalPropertyEight_helper(c, e, h);
    },
    notAddAdditionalPropertyEight: function (c, e, h){
       var insuranceProduct = c.get("v.insuranceProduct");
       var selectedValue  = e.getSource().get("v.label");
       insuranceProduct.CS_Additional_property__c = selectedValue;
       c.set("v.insuranceProduct",insuranceProduct);
    },
    addAnotherInsuredFifth: function (c, e, h){
        h.addAnotherInsuredFifth_helper(c, e, h);
    },

    cpoyBillingToMailing: function (c, e, h){
        h.cpoyBillingToMailing_helper(c, e, h);
    },
    saveQuoteRecord: function (c, e, h){
       h.saveQuoteRecord_helper(c, e, h);
    },


})